<p align="center">
  <img alt="bloom logo" src="assets/icons/bloom_256.png" height="180" />
  <h3 align="center">Bloom</h3>
  <p align="center">A safe place for all your data (Android, IOS, Linux, MacOS, CLI)</p>
</p>

You no longer trust tech monopolies with your data?<br/>
You are done with your privacy invaded by advertisers? <br/>
You are tired of being abused by Big Companies?

**We too, so we built Bloom.**

Bloom is an open source and encrypted productivity suite. It features Files, Notes, Calendar, Contacts and
much more.

[Try it for free](https://bloom.sh/download)


Android: Coming soon <!--[Google play store](https://play.google.com/store/apps/details?id=com.bloom42.bloomx) --> <br/>
iOS: Coming soon

Linux: Coming soom <br/>
MacOS: Coming soon <br/>
Docker: Coming soon <br/>
CLI: Coming soom <br/><!-- [See instructions](https://help.bloom.sh/en/article/how-to-install-the-command-line-client-b9350j) <br /> -->
<!-- Windows: Coming soon <br/> -->


--------

Why? How? What? 👉 Read the launch post: https://fatalentropy.com/bloom-a-free-and-open-source-google

1. [Documentation](#documentation)
2. [Roadmap](#roadmap)
3. [Development instructions](#development-instructions)
4. [Contributing](#contributing)
5. [Community](#Community)
6. [Licensing](#licensing)
7. [Security](#security)

--------


## Documentation

See the wiki: https://gitlab.com/bloom42/bloom/-/wikis/home


## Roadmap

See https://gitlab.com/bloom42/bloom/-/milestones


## Development instructions

See https://gitlab.com/bloom42/bloom/-/wikis/development


## Community

<!-- Join our Reddit community: https://www.reddit.com/r/bloom42 -->

Please use [issues](https://gitlab.com/bloom42/bloom/-/issues) if you have some suggestions or want to report bugs.


You can follow us on [Twitter @42bloom](https://twitter.com/@42bloom) or on [Mastdon @42bloom@mastodon.social](https://mastodon.social/@42bloom) to never miss an update.



## Contributing

Thank you for your interest in contributing! Please refer to
https://gitlab.com/bloom42/wiki/-/wikis/organization/contributing for guidance.


## Licensing

See `LICENSE.txt` and https://bloom.sh/licensing


## Security

You can read the details of our security architecture here: https://gitlab.com/bloom42/bloom/-/wikis/security.

If you found a security issue affecting this project, please do not open a public issue and refer to our
[dedicated security page](https://bloom.sh/security) instead. Thank you!
