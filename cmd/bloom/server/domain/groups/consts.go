package groups

const (
	RoleMember        = "MEMBER"
	RoleAdministrator = "ADMINISTRATOR"

	GROUP_NAME_MAX_LENGTH        = 64
	GROUP_NAME_MIN_LENGTH        = 4
	GROUP_DESCRIPTION_MAX_LENGTH = 300
)
