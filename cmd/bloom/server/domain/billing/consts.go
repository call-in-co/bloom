package billing

const (
	DefaultStorage                  = 1000000000 // 1GB
	DefaultParallelBitflowDownloads = 0
	DefaultBillingPlanId            = "42fb1c42-caca-418d-81f3-a6313c4a0a42"

	BILLING_PLAN_NAME_MAX_LENGTH = 15
	BILLING_PLAN_NAME_MIN_LENGTH = 3
)
